# flatpak-dedup-checker
flatpak-dedup-checker is a utility to check the amount of data used in a
Flatpak directory.

flatpak-dedup-checker checks and contrasts the size between pre-deduplication, post-deduplication, and compression if using btrfs. Do note that directories with btrfs require elevated privileges to run the `compsize` command.

## List of Options
```
Usage: flatpak-dedup-checker [options] COMMAND

-h, --help		Print the help section and exit
-p PATH, --path=PATH	Specify custom path (default: /var/lib/flatpak)
-r, --runtime		List data only used by runtimes
-a, --app		List data only used by applications
--user			Work on the user installation
```

## Examples
flatpak-dedup-checker with defaults:
```
$ ./flatpak-dedup-checker
Directories                 /var/lib/flatpak/{runtime,app}
Size without deduplication: 8.04 GB
Size with deduplication:    6.14 GB (76% of 8.04 GB)
Size with compression:      4.09 GB (50% of 8.04 GB; 66% of 6.14 GB)
```

flatpak-dedup-checker with custom directory:
```
$ ./flatpak-dedup-checker --path=~/.local/share/flatpak
Directories                 /var/home/TheEvilSkeleton/.local/share/flatpak/{runtime,app}
Size without deduplication: 11.57 GB
Size with deduplication:    8.88 GB (76% of 11.57 GB)
Size with compression:      6.91 GB (59% of 11.57 GB; 77% of 8.88 GB)
```

flatpak-dedup-checker with custom directory with information about runtimes only:
```
$ ./flatpak-dedup-checker --path=~/.local/share/flatpak --runtime
Directory:                  /var/home/TheEvilSkeleton/.local/share/flatpak/runtime
Size without deduplication: 6.41GB
Size with deduplication:    3.94 GB (61% of 6.41 GB)
Size with compression:      2.61 GB (40% of 6.41 GB; 66% of 3.94 GB)
```

